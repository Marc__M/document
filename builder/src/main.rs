use std::fs::File;
use std::io::Write;
use std::path::Path;

mod information;

use information::Information;

type Function = fn(args: &[String]) -> bool;

struct Command {
    name: String,
    command: Function
}

fn init(_args: &[String]) ->bool {
    let mut file = File::create("Builder.toml").unwrap();
    file.write_all(b"[information]\n").unwrap();
    file.write_all(b"type=\"article\"\n").unwrap();
    file.write_all(b"title=\"Title\"\n").unwrap();
    file.write_all(b"author=\"author\"\n").unwrap();
    file.write_all(b"date=\"date\"\n").unwrap();
    file.write_all(b"\n\n[dependencies]").unwrap();

    std::fs::create_dir("document").unwrap();
    let mut main = File::create("document/main.doc").unwrap();
    main.write_all(b"\\begin{document}").unwrap();
    main.write_all(b"\\end{document}").unwrap();
    true
}

fn create(args: &[String]) -> bool {
    if args.len() >= 3 {
        std::fs::create_dir(args[2].as_str()).unwrap();
        std::env::set_current_dir(Path::new(&args[2].as_str())).unwrap();
        init(args);
        true
    } else {
        false
    }
}

fn build(_args: &[String]) -> bool {
    let info = std::fs::read_to_string("Builder.toml").unwrap();
    let informations = Information::new(&info);
    informations.download_dependencies();
    informations.exploit();
    true
}

fn main() {
    let args: Vec<_> = std::env::args().collect();
    let commands = vec![
        Command {name: String::from("init"), command: init},
        Command {name: String::from("create"), command: create},
        Command {name: String::from("build"), command: build}
    ];

    if args.len() < 2 {
        println!("No given action");
    } else {
        for command in commands {
            if command.name == args[1] {
                let function = command.command;
                if function(&args) {
                    return;
                }
            }
        }
    }
}
