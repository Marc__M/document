#[derive(Debug)]
pub struct Dependencie {
    name: String,
    version: String
}

#[derive(Debug)]
pub struct Information {
    doc_type: String,
    doc_title: String,
    doc_author: String,
    doc_date: String,

    dependencies: Vec<Dependencie>
}

impl Information {
    pub fn new(info: &str) -> Information {
        let lines: Vec<_> = info.split('\n').collect();

        let mut info = Information {
            doc_type: String::new(),
            doc_title: String::new(),
            doc_author: String::new(),
            doc_date: String::new(),
            dependencies: Vec::new()
        };

        let mut i=0;
        while i < lines.len() {
            if lines[i] == "[information]" {
                i+= 1;

                while i < lines.len() && lines[i] != "[dependencies]" {
                    if lines[i].contains("type") {
                        let data: Vec<_> = lines[i].split('=').collect();
                        let data = data[1].trim();
                        let data = &data[1..data.len()-1];

                        info.doc_type = data.to_string();
                    } else if lines[i].contains("title") {
                        let data: Vec<_> = lines[i].split('=').collect();
                        let data = data[1].trim();
                        let data = &data[1..data.len()-1];

                        info.doc_title = data.to_string()
                    } else if lines[i].contains("author") {
                        let data: Vec<_> = lines[i].split('=').collect();
                        let data = data[1].trim();
                        let data = &data[1..data.len()-1];

                        info.doc_author = data.to_string();
                    } else if lines[i].contains("date") {
                        let data: Vec<_> = lines[i].split('=').collect();
                        let data = data[1].trim();
                        let data = &data[1..data.len()-1];

                        info.doc_date = data.to_string();

                    }
                    i+= 1
                }
            }

            if lines[i] == "[dependencies]" {
                i+= 1;

                while i < lines.len() && lines[i] != "[information]" {
                    if !lines[i].is_empty() {
                        let data: Vec<_> = lines[i].split('=').collect();
                        let dep = Dependencie {
                            name: data[0].trim().to_string(),
                            version: data[1].trim().to_string() 
                        };
                        info.dependencies.push(dep);
                    }
                    i+= 1
                }
            }
            i+=1;
        }

        println!("{0:#?}", info);
        info
    }

    pub fn download_dependencies(self: &Information) {
    }

    pub fn exploit(self: &Information) {
    }
}
