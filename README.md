# Document
A document edition system made to replace LaTeX

## Architecture
The document system is build of three parts:
- The `builder` tool
- The `documentpdf` tool (not started yet)
- The `documentc` tool (not started yet)

### Builder
Builder is a tool ispired by `cargo` for rust, who will automaticly download
dependencies of your document.

### documentpdf
The equivalent of `pdflatex` build the document from the text input to the pdf
output format

### documentc
A simple script programming language for use in the documents.
